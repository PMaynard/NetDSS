import opendssdirect as dss
from opendssdirect.utils import run_command

dss.run_command('Redirect simple.dss');

print("[-] Capacitors\n", dss.utils.capacitors_to_dataframe())
print("[-] Fuses\n", dss.utils.fuses_to_dataframe())
print("[-] Generators\n", dss.utils.generators_to_dataframe())
print("[-] Isource\n", dss.utils.isource_to_dataframe())
print("[-] Lines\n", dss.utils.lines_to_dataframe())
print("[-] Loads\n", dss.utils.loads_to_dataframe())
print("[-] Loadshape\n", dss.utils.loadshape_to_dataframe())
print("[-] Meters\n", dss.utils.meters_to_dataframe())
print("[-] Monitors\n", dss.utils.monitors_to_dataframe())
print("[-] Pvsystems\n", dss.utils.pvsystems_to_dataframe())
print("[-] Reclosers\n", dss.utils.reclosers_to_dataframe())
print("[-] Regcontrols\n", dss.utils.regcontrols_to_dataframe())
print("[-] Relays\n", dss.utils.relays_to_dataframe())
print("[-] Sensors\n", dss.utils.sensors_to_dataframe())
print("[-] Transformers\n", dss.utils.transformers_to_dataframe())
print("[-] Vsources\n", dss.utils.vsources_to_dataframe())
print("[-] Xycurves\n", dss.utils.xycurves_to_dataframe())