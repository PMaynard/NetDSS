# NetDSS

A Network Distribution System Simulator (NetDSS) based on EPRI's OpenDSS.

## Install

	python3 -m venv venv         # Create a virtual environment
	. venv/bin/activate
	pip install 'OpenDSSDirect.py[extras]'

